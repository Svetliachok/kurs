<?php

namespace System;

use PDO;

class Model {
    
    private string $host;
    private string $db;
    private string $user;
    private string $pass;
    private string $charset = 'utf8';
    public PDO $pdo;
    protected string $table;
    
    public function __construct() {
        $this->host = '127.0.0.1';
        $this->db = 'kurs';
        $this->user = 'mysql';
        $this->pass = 'mysql';

        $dsn = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";
        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ];
        $this->pdo = new PDO($dsn, $this->user, $this->pass, $opt);
    }
    
    public function getAll(): array
    {
        $stmt = $this->pdo->prepare('SELECT * FROM ' . $this->table);
        $stmt->execute();

        $data = [];
        foreach ($stmt as $row)
        {
            $data[] = $row['name'];
        }

        return $data;
    }
}
