<?php

namespace System;

class View {
    public static function render(string $path, array $data = []) {
        //Формируем полный путь к виду
        $fullPath = __DIR__ . '/../Views/' . $path . '.php';
        //Проверяем наличи файла
        if(!file_exists($fullPath)) {
            throw new \Exception('File does not exists');
        }
        //Преобразуем массив
        if(!empty($data)) {
            foreach ($data as $key => $value) {
                $$key = $value;
            }
        }
        
        require_once $fullPath;
    }
}
